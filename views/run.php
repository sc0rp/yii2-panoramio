<?php if ($transparentBackground): ?>
<style type='text/css'>
    .panoramio-wapi-photo .panoramio-wapi-images {
        background:transparent;
    }
</style>
<?php
endif;

$js = <<<JS
    var myRequest;
    var widget;
    $(document).ready(function () {
        myRequest = new panoramio.PhotoRequest({
            'tag': '{$tag}'
        });

        width = $("#{$id}").width();
        height = parseInt(0.75 * width);

        $("#{$id}").css('height', height + 'px');
        var myOptions = {
            'width': width,
            'height': height,
            'croppedPhotos': false,
            'orientation': 'horizontal'
        };
        widget = new panoramio.PhotoWidget('{$id}', myRequest, myOptions);
        widget.setPosition(0);

        setInterval(function() {
            if (!widget.getAtEnd()) {
                index = widget.getPosition();
                widget.setPosition(index+1);
            } else {
                widget.setPosition(0);
            }
        },5000);
    });
JS;
$this->registerJs($js, $this::POS_END);
?>
<div id="<?php echo $id;?>"></div>

