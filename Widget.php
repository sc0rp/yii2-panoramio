<?php

namespace sc0rp\panoramio;
use sc0rp\panoramio\assets\Assets;
use Yii;

/**
 * This is just an example.
 */
class Widget extends \yii\base\Widget
{

    public $predefinedTags = [
        'sunset',
        'icy',
        'sea',
        'jungle',
        'forest',
        'roads',
        'statue',
        'monument',
        'sunrise',
        'sunshine',
        'city',
        'abstract',
        'river',
        'japan',
        'usa',
        'monument',
        'girl',
        'woman',
        'chick',
        'car',
        'motor'
    ];

    public $tag;
    public $id;
    public $transparentBackground = true;

    public function init() {
        Assets::register($this->view);
        if ($this->tag == null) {
            $this->tag = $this->predefinedTags[rand(0,count($this->predefinedTags)-1)];
        }

        if ($this->id == null) {
            $this->id = 'panoramio';// . Yii::$app->security->generateRandomString();
        }
        return parent::init();
    }

    public function run()
    {
        return $this->render('run', [
            'tag' => $this->tag,
            'id' => $this->id,
            'transparentBackground' => $this->transparentBackground
        ]);
    }
}
