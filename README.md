Panoramio widget for Yii2
=========================
This is Panoramio widget for Yii2.

Installation
------------

The preferred way to install this extension is through [composer](http://getcomposer.org/download/).

Either run

```
php composer.phar require --prefer-dist sc0rp/yii2-sc0rp-panoramio "*"
```

or add

```
"sc0rp/yii2-sc0rp-panoramio": "*"
```

to the require section of your `composer.json` file.


Usage
-----

Once the extension is installed, simply use it in your code by  :

```php
<?= \sc0rp\panoramio\AutoloadExample::widget(); ?>```