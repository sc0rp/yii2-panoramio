<?php

namespace sc0rp\panoramio\assets;

use \yii\web\AssetBundle;

class Assets extends AssetBundle {
    public $sourcePath = '@vendor/sc0rp/yii2-sc0rp-panoramio/assets';
    public $baseUrl = '@web';

    /*
    public $css = [
        'css/style.css'
    ];
    */
    public $js = [
        'http://www.panoramio.com/wapi/wapi.js?v=1&amp;hl=en'
    ];
    public $depends = [
        'yii\web\JqueryAsset'
    ];

    public function init() {
        parent::init();
    }

}